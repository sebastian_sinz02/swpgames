package at.test;

public class Titel {
    private String songName;
    private int songLength;

    public Titel(String songName, int songLength) {
        this.songName = songName;
        this.songLength = songLength;
    }

    public String getSongName() {
        return songName;
    }

    public int getSongLength() {
        return songLength;
    }


}
