package at.test;

import java.util.ArrayList;
import java.util.List;

public class Schallplatte {
    private int id;
    private String Plattentitel;
    private List<Titel> titels;
    double songLength;

    public Schallplatte(int id, String plattentitel) {
        this.id = id;
        Plattentitel = plattentitel;
        this.titels = new ArrayList<>();
    }

    public void addSong(Titel titel) {
        this.titels.add(titel);
    }
    public String getPlattentitel() {
        return Plattentitel;
    }
    public void getSumOfMusic(){
        int songLength = 0;
        for (Titel titel: titels){
            songLength = songLength + titel.getSongLength();
        }
        this.songLength = songLength;
    }
     public int getSongLength(){
        getSongLength();
        return songLength;
     }



}


