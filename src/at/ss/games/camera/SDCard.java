package at.ss.games.camera;

import java.util.List;

public class SDCard {
    private int capacity;
    private List<Photofiles> files;

    public SDCard(int capacity) {
        this.capacity = capacity;
    }

    public void save(Photofiles file){
        files.add(file);
    }

    public List<Photofiles> getFiles() {
        return files;
    }
}
