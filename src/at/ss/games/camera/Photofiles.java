package at.ss.games.camera;

public class Photofiles {
    private String extension;
    private int size;
    private String name;

    public Photofiles(String extension, int size, String name) {
        this.extension = extension;
        this.size = size;
        this.name = name;
    }

    public String getExtension() {
        return extension;
    }

    public int getSize() {
        return size;
    }

    public String getName() {
        return name;
    }
}
