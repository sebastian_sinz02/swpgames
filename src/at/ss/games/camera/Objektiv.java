package at.ss.games.camera;

public class Objektiv {
    private String producer;
    private int brennweite;

    public Objektiv(String producer, int brennweite) {
        this.producer = producer;
        this.brennweite = brennweite;
    }

    public String getProducer() {
        return producer;
    }

    public int getBrennweite() {
        return brennweite;
    }
}
