package at.ss.games.camera;

import java.util.List;

public class Camera {
    private int pixel;
    private String color;
    private String gewicht;
    private SDCard sdCard;
    private Trigger trigger;
    private Producer producer;
    private Objektiv objektiv;

    public Camera(int pixel, String color, String gewicht, SDCard sdCard, Trigger trigger, Producer producer, Objektiv objektiv) {
        this.pixel = pixel;
        this.color = color;
        this.gewicht = gewicht;
        this.sdCard = sdCard;
        this.trigger = trigger;
        this.producer = producer;
        this.objektiv = objektiv;
    }

    public void takePic() {
        Photofiles file = this.trigger.takePic();
        this.sdCard.save(file);
    }

    public void printAllFiles(){
        List<Photofiles> files = this.sdCard.getFiles();
        for (Photofiles file : files){
            System.out.println(file.getName());
        }
    }
}
