package at.ss.games.camera;

public class Producer {
    private String name;
    private String land;

    public Producer(String name, String land) {
        this.name = name;
        this.land = land;
    }

    public String getName() {
        return name;
    }

    public String getLand() {
        return land;
    }
}
