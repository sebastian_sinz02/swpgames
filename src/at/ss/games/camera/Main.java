package at.ss.games.camera;

public class Main {
    public static void main(String[] args) {
        Objektiv objektiv = new Objektiv("canon", 50);
        Producer producer = new Producer("canon", "AT");
        SDCard sdCard = new SDCard(12);
        Trigger trigger = new Trigger();

        Camera camera = new Camera(800, "red", "125g", sdCard, trigger, producer, objektiv );
        camera.takePic();
        camera.takePic();
        camera.takePic();
        camera.printAllFiles();



    }
}
