package at.ss.games.mygame;

import org.newdawn.slick.*;

public class AstronautinSpace extends BasicGame {



    public AstronautinSpace(String astronautinspace) {
        super("Astronaut in Space");
    }

    private Background background;
    private Astronaut astronaut;
    private Meteor meteor;

    @Override
    public void init(GameContainer gameContainer) throws SlickException {


        astronaut = new Astronaut();
        background = new Background();
        meteor = new Meteor();

    }

    @Override
    public void update(GameContainer gameContainer, int i) throws SlickException {
        this.background.update(gameContainer, i);
        this.astronaut.update(gameContainer, i);
        this.meteor.update(gameContainer, i);

    }

    @Override
    public void render(GameContainer gameContainer, Graphics graphics) throws SlickException {

        background.render(graphics);
        astronaut.render(graphics);
        meteor.render(graphics);
    }

    @Override
    public void keyPressed(int key, char c) {
        System.out.println(key);
    }

    public static void main(String[] argv) {
        try {
            AppGameContainer container = new AppGameContainer(new AstronautinSpace("Astronaut"));
            container.setDisplayMode(700, 1244, false);
            container.start();
        } catch (SlickException e) {
            e.printStackTrace();
        }
    }
}