package at.ss.games.mygame;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.SlickException;


import java.util.Random;

public class Meteor implements Actor {
    private Image meteorImage;
    private float x, y;
    private Random random;
    private int speed;

    public Meteor() throws SlickException {
        this.random = new Random();
        Image tmp = new Image("testdata/meteor.png");
        this.meteorImage = tmp.getScaledCopy(88, 70);
        setRandomPosition();
    }

    public void setRandomPosition() {
        this.x = this.random.nextInt(800);
        this.y = this.random.nextInt(600) - 600;
    }

    @Override
    public void render(Graphics graphics) {
        meteorImage.draw();
    }


    @Override
    public void update(GameContainer gameContainer, int delta) {
        this.y += (float)delta/this.speed;
        if (this.y > 600){
            setRandomPosition();
        }
    }
}
