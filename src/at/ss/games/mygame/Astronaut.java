package at.ss.games.mygame;

import org.newdawn.slick.*;

public class Astronaut implements Actor {
    private Image astronautImage;
    private float x, y;

    public Astronaut() throws SlickException {
        Image tmp = new Image("testdata/astronaut.png");
        this.astronautImage = tmp.getScaledCopy(50, 60);
        this.x = 330;
        this.y = 1100;

    }

    @Override
    public void render(Graphics graphics) {
        astronautImage.draw(this.x, this.y);
    }


    @Override
    public void update(GameContainer gameContainer, int delta) {
        if (gameContainer.getInput().isKeyDown(Input.KEY_RIGHT)) {
            this.x++;
        }
        if (gameContainer.getInput().isKeyDown(Input.KEY_LEFT)) {
            this.x--;
        }
        if (gameContainer.getInput().isKeyDown(Input.KEY_UP)) {
            this.y--;
        }
        if (gameContainer.getInput().isKeyDown(Input.KEY_DOWN)) {
            this.y++;
        }
    }
}
