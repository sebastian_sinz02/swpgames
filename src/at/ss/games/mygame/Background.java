package at.ss.games.mygame;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.SlickException;

public class Background implements Actor {
    private Image backgroundImage;

    public Background() throws SlickException {
        this.backgroundImage = new Image("testdata/space.png");
    }

    @Override
    public void render(Graphics graphics) {
        backgroundImage.draw(0, 0);

    }

    @Override
    public void update(GameContainer gameContainer, int delta) {

    }
}