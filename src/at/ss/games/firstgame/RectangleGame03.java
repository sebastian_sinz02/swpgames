package at.ss.games.firstgame;

import org.newdawn.slick.*;
public class RectangleGame03 extends BasicGame {
    private float speed;

    private float xRectangle;
    private float yRectangle;

    private float xCircle;
    private float yCircle;

    private float xOval;
    private float yOval;

    private int rectangleSpeed = 1;
    private int circleSpeed = 1;
    private int ovalSpeed = 1;


    public RectangleGame03(String title) {
        super(title);
    }

    @Override
    public void init(GameContainer gameContainer) throws SlickException {
        this.xRectangle = 100;
        this.yRectangle = 100;

        this.xCircle = 0;
        this.yCircle = 100;

        this.xOval = 100;
        this.yOval = 0;

        this.speed = 2.0f;
    }

    @Override
    public void update(GameContainer gameContainer, int delta) throws SlickException {
        if (rectangleSpeed == 1){
            this.xRectangle += (float)delta/this.speed;
            if (this.xRectangle >= 600) {
                this.xRectangle += (float) delta * 0;
                this.xRectangle = 600;
                this.rectangleSpeed = 2;
            }
        }
        if (rectangleSpeed == 2) {
            this.yRectangle += (float) delta / speed;
            if (this.yRectangle >= 600) {
                this.yRectangle += (float) delta * 0;
                this.yRectangle = 600;
                this.rectangleSpeed = 3;
            }
        }
        if (rectangleSpeed == 3) {
            this.xRectangle -= (float) delta / speed;
            if (this.xRectangle <= 100) {
                this.xRectangle += (float) delta * 0;
                this.xRectangle = 100;
                this.rectangleSpeed = 4;
            }
        }

        if (rectangleSpeed == 4) {
            this.yRectangle -= (float) delta / speed;
            if (this.yRectangle <= 100) {
                this.yRectangle += (float) delta * 0;
                this.yRectangle = 100;
                this.rectangleSpeed = 1;
            }
        }

        if(circleSpeed == 1){
            this.yCircle += (float) delta / speed;
            if(this.yCircle>=600){
                this.yCircle += (float) delta * 0;
                this.yCircle = 600;
                this.circleSpeed = 2;
            }
        }

        if(circleSpeed == 2){
            this.yCircle -= (float) delta / speed;
            if (this.yCircle <= 100){
                this.yCircle += (float) delta * 0;
                this.yCircle = 100;
                this.circleSpeed = 1;
            }
        }

        if(ovalSpeed == 1){
            this.xOval += (float) delta / speed;
            if(this.xOval>=600){
                this.xOval += (float) delta * 0;
                this.xOval = 600;
                this.ovalSpeed = 2;
            }
        }

        if(ovalSpeed == 2){
            this.xOval -= (float) delta / speed;
            if(this.xOval<=100){
                this.xOval += (float) delta * 0;
                this.xOval = 100;
                this.ovalSpeed = 1;
            }
        }


    }

    @Override
    public void render(GameContainer gameContainer, Graphics graphics) throws SlickException {
        graphics.drawRect(this.xRectangle, this.yRectangle, 100, 100);
        graphics.drawOval(this.xCircle, this.yCircle, 50, 50);
        graphics.drawOval(this.xOval, this.yOval, 100, 50);
    }
    public static void main(String[] argv) {
        try {
            AppGameContainer container = new AppGameContainer(new RectangleGame03("Rectangles"));
            container.setDisplayMode(800, 800, false);
            container.start();
        } catch (SlickException e) {
            e.printStackTrace();
        }
    }
}
