package at.ss.games.firstgame;

import org.newdawn.slick.AppGameContainer;
import org.newdawn.slick.SlickException;

public class mygame {

    public static void main(String[] argv) {
        try {
            AppGameContainer container = new AppGameContainer(new RectangleGame03("Rectangles"));
            container.setDisplayMode(800, 800, false);
            container.start();
        } catch (SlickException e) {
            e.printStackTrace();
        }
    }
}