package at.ss.games.firstgame;

import org.newdawn.slick.Graphics;

import java.util.Random;

public class Circle04 implements Actor {

    private int x = random.nextInt(800);//0-799
    private float y = random.nextInt(600); //0-599
    private int speed = random.nextInt(40) + 10; //10-49
    private int diameter = random.nextInt(20) + 20; //20-39


    @Override
    public void render(Graphics graphics){
        graphics.drawOval(this.x, this.y, this.diameter, this.diameter);

    }

    public void update(int delta){
        this.y += (float)delta/this.speed;
        if (this.y>600+this.diameter){
            this.y = -diameter;
        }
        this.diameter = (int) this.y/10 + 20;
    }
}
