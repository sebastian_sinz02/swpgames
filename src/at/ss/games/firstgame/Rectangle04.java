package at.ss.games.firstgame;

import org.newdawn.slick.Graphics;

public class Rectangle04 implements Actor{

    private Direction direction;
    private float x;
    private float y;
    private float speed;

    public Rectangle04(int x, int y, int speed, Direction direction) {
        this.x = x;
        this.y = y;
        this.speed = speed;
        this.direction = direction;
    }


    @Override
    public void render(Graphics graphics){
        graphics.drawRect(this.x, this.y, 30, 30);

    }

    public void update(int delta){

        switch (this.direction) {
            case RIGHT:
                this.x += (float)delta/this.speed;
                break;
            case DOWN:
                this.y += (float)delta/this.speed;
                break;
            case LEFT:
                this.x -= (float)delta/this.speed;
                break;
            case UP:
                this.y -= (float)delta/this.speed;
                break;
        }
        if (this.x>800){
            this.x = 0;
        }
    }

    public static enum Direction {
        RIGHT,
        DOWN,
        LEFT,
        UP
    };
}
