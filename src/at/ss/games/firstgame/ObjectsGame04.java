package at.ss.games.firstgame;

import org.newdawn.slick.*;

import javax.swing.plaf.basic.BasicSplitPaneUI;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class ObjectsGame04 extends BasicGame {

    private List<Actor> actors;




    public ObjectsGame04(String title) {
        super(title);
    }

    @Override
    public void init(GameContainer gameContainer) throws SlickException {
        this.actors = new ArrayList<>();

        Random random = new Random();
        for (int i = 0; i < 5; i++){
            Rectangle04 rectangle = new Rectangle04(random.nextInt(600), random.nextInt(600), random.nextInt(50), Rectangle04.Direction.RIGHT);
            actors.add(rectangle);
        }
        for (int i = 0; i < 5; i++){
            Circle04 circle = new Circle04();
            this.actors.add(circle);
        }
        for (int i = 0; i < 5; i++) {
            Ellipse04 ellipse = new Ellipse04(random.nextInt(800), random.nextInt(600), random.nextInt(50));
            this.actors.add(ellipse);
        }
    }

    @Override
    public void update(GameContainer gameContainer, int delta) throws SlickException {
        for (Actor actor:this.actors) {
            actor.update(delta);
        }
    }

    @Override
    public void render(GameContainer gameContainer, Graphics graphics) throws SlickException {
        for (Actor actor:this.actors) {
            actor.render(graphics);
        }
    }
    public static void main(String[] argv) {
        try {
            AppGameContainer container = new AppGameContainer(new ObjectsGame04("Rectangles"));
            container.setDisplayMode(800, 800, false);
            container.start();
        } catch (SlickException e) {
            e.printStackTrace();
        }
    }
}
