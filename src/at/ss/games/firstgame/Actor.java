package at.ss.games.firstgame;

import org.newdawn.slick.Graphics;

import java.util.Random;

public interface Actor {
    public void render(Graphics graphics);
    public void update(int delta);

    Random random = new Random();
}
