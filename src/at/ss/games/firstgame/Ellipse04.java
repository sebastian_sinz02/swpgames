package at.ss.games.firstgame;

import org.newdawn.slick.Graphics;

public class Ellipse04 implements Actor {

    private int x, y;
    private float speed;

    public Ellipse04(int x, int y, float speed) {
        this.x = x;
        this.y = y;
        this.speed = 2;
    }

    @Override
    public void render(Graphics graphics) {
        graphics.drawOval(this.x,this.y, 50, 100);
    }

    @Override
    public void update(int delta) {
        this.x += (float)delta/this.speed;
        this.y += (float)delta/this.speed;
        if (this.x>800){
            this.x = 0;
        }else if (this.y>600){
            this.y = 0;
        }
    }
}

