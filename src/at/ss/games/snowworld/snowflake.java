package at.ss.games.snowworld;

import org.newdawn.slick.Graphics;

import java.util.Random;

public class snowflake implements Actor {
     public enum SIZE {BIG, SMALL, MEDIUM};
     private int size;
     private int speed;
     private float x,y;
     private Random random;

     public snowflake(SIZE size) {
         this.random = new Random();
         if (size == SIZE.BIG){
             this.size = 16;
             this.speed = 1;
         }
         if (size == SIZE.MEDIUM){
             this.size = 11;
             this.speed = 5;
         }
         if (size == SIZE.SMALL){
             this.size = 5;
             this.speed = 10;
         }

         setRandomPosition();
     }

     public void setRandomPosition(){
        this.x = this.random.nextInt(800);
        this.y = this.random.nextInt(600) - 600;
    }

    @Override
    public void render(Graphics graphics){
         graphics.fillOval(this.x, this.y, this.size, this.size);
    }

    @Override
    public void update(int delta){
         this.y += (float)delta/this.speed;
         if (this.y > 600){
             setRandomPosition();
         }
    }
}


