package at.ss.games.phone;

public class Sim {
    private int ID;
    private String number;


    public Sim(int ID, String number) {
        this.ID = ID;
        this.number = number;
    }

    public int getID() {
        return ID;
    }

    public String getNumber() {
        return number;
    }



    public void sim() {
        System.out.println("ID of my SIM: " + this.getID() + " this is my number: " + this.getNumber());
    }

    public void doCall(String number) {
        System.out.println("calling...................." + number);
        }
    }

