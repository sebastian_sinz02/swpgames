package at.ss.games.phone;

public class Phone {
    private String color;
    private Sim sim;
    private Camera camera;
    private SDCard sdcard;

    public Phone(String color, Sim sim, Camera camera, SDCard sdcard) {
        this.color = color;
        this.sim = sim;
        this.camera = camera;
        this.sdcard = sdcard;
    }

    public String getColor() {
        return color;
    }

    public Phone setColor(String color) {
        this.color = color;
        return this;
    }

    public Sim getSim() {
        return sim;
    }

    public Phone setSim(Sim sim) {
        this.sim = sim;
        return this;
    }

    public Camera getCamera() {
        return camera;
    }

    public Phone setCamera(Camera camera) {
        this.camera = camera;
        return this;
    }

    public SDCard getSdcard() {
        return sdcard;
    }

    public Phone setSdcard(SDCard sdcard) {
        this.sdcard = sdcard;
        return this;
    }
    public void Sim(){
        sim.sim();
    }

    public void makePicture() {
        camera.makePicture();
    }

    public void doCall(String number) {
        sim.doCall(number);
    }

}
