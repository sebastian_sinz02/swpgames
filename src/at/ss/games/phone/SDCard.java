package at.ss.games.phone;

import java.util.ArrayList;

public class SDCard {
    private int capacity;
    private PhoneFile phoneFile;
    private ArrayList<PhoneFile> phoneFiles = new ArrayList<PhoneFile>();

    public SDCard(int capacity) {
        this.capacity = capacity;
    }

    public int getCapacity() {
        return capacity;
    }

    public void showPhoneFiles() {
        System.out.println(this.phoneFiles);
    }

    public void savePhoneFile(PhoneFile phoneFile) {
        this.phoneFile = phoneFile;
        this.phoneFiles.add(phoneFile);
    }

    public void getAllPhoneFiles() {

        for (int i = 0; i < phoneFiles.size(); i++) {
            System.out.println("PhoneFile " + (i + 1));
            System.out.println(this.phoneFiles.get(i).getName());
            System.out.println(this.phoneFiles.get(i).getSize());
            System.out.println(this.phoneFiles.get(i).getExtension());
        }
    }

    public PhoneFile getPhoneFile(int i) {
        return this.phoneFiles.get(i);
    }

    public void getFreeSpace() {
        int freeSpace = capacity;
        for (int i = 0; i < phoneFiles.size(); i++) {
            freeSpace = freeSpace - this.phoneFiles.get(i).getSize();
        }
        System.out.println("free Space: " + freeSpace + "MB");
    }

}
