package at.ss.games.phone;

public class PhoneFile {
    private String extension;
    private int size;
    private String name;

    public PhoneFile(String extension, int size, String name) {
        this.extension = extension;
        this.size = size;
        this.name = name;
    }

    public String getExtension() {
        return extension;
    }

    public PhoneFile setExtension(String extension) {
        this.extension = extension;
        return this;
    }

    public int getSize() {
        return size;
    }

    public PhoneFile setSize(int size) {
        this.size = size;
        return this;
    }

    public String getName() {
        return name;
    }

    public PhoneFile setName(String name) {
        this.name = name;
        return this;
    }

    public boolean getInfo() {
        System.out.println("Extension is: " + this.getExtension()
                + " Name is: " + this.getName()
                + " Size of SD: " + this.getSize());
        return false;
    }

}
